// Cau lenh nay tuong tu cau lenh import express form; dung de import thu vien express vao project
const express = require("express");

//import thu vien path
const path = require("path")

// Khởi tạo app expre
const app = express();

// Khai bao cổng của project 
const port = 8000;

// Khai bao API dang GET "/" sẽ chạy vào đây
// Callback function: là 1 tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get ("/", (request, response ) => {
        console.log (__dirname);
        response.sendFile(path.join(__dirname +'/views/example-call.html'));
    })

    app.get ("/", (request, response ) => {
        console.log (__dirname);
        response.sendFile(path.join(__dirname +'/views/pizza365.html'));
    })
// chay app lắng nghe
app.listen(port, () => {
    console.log("App listerning on port: " + port)
})